/**
 * Creates and AT Internet SmartTag and dispatches it.
 */
(($, Drupal, once, drupalSettings) => {
  // Process only if atsmarttag settings have been defined.
  if (drupalSettings.atsmarttag === undefined) {
    return;
  }

  Drupal.atsmarttag = {
    _instantiateTag(settings) {
      const tagProperties = {};
      if (settings.site) {
        tagProperties.site = settings.site;
      }
      if (settings.collect_domain) {
        tagProperties.collectDomain = settings.collect_domain;
      }
      if (settings.collect_domain_ssl) {
        tagProperties.collectDomainSSL = settings.collect_domain_ssl;
      }
      if (settings.secure !== 0) {
        tagProperties.secure = true;
      }
      if (settings.disable_cookie !== 0) {
        tagProperties.disableCookie = true;
      }
      if (settings.cookie_domain) {
        tagProperties.cookieDomain = settings.cookie_domain;
      }
      window.ATTag = new window.ATInternet.Tracker.Tag(tagProperties);
    },

    _configurePrivacy(settings) {
      if (window.ATTag.privacy) {
        if (settings.cnil_exempt !== 0) {
          window.ATTag.privacy.setVisitorMode('cnil', 'exempt');
        }
      }
    },

    _addChaptersToProperties(settings, properties) {
      if (settings.page_chapter1) {
        properties.chapter1 = settings.page_chapter1;
      }
      if (settings.page_chapter2) {
        properties.chapter2 = settings.page_chapter2;
      }
      if (settings.page_chapter3) {
        properties.chapter3 = settings.page_chapter3;
      }
    },

    _processPage(settings) {
      const pageProperties = {};
      if (settings.page_name) {
        pageProperties.name = settings.page_name;
      }
      this._addChaptersToProperties(settings, pageProperties);
      if (Object.getOwnPropertyNames(pageProperties).length > 0) {
        window.ATTag.page.set(pageProperties);
        window.ATTag.dispatch();
      }
    },

    _registerActionListeners() {
      const self = this;
      // Attach mousedown, keyup, touchstart events to document only and catch
      // clicks on all elements.
      $(document.body).on('mousedown keyup touchstart', (event) => {
        // Catch the closest surrounding link of a clicked element.
        $(event.target)
          .closest('a,area')
          .each(function processLinks() {
            const link = this;
            // Is the clicked URL internal?
            if (Drupal.atsmarttag.isInternal(link.href)) {
              // Is download tracking activated and the file extension configured
              // for download tracking?
              if (
                drupalSettings.atsmarttag.trackDownload &&
                Drupal.atsmarttag.isDownload(link.href)
              ) {
                // Download link clicked.
                const clickProperties = {
                  elem: link,
                  name: link.href,
                  type: 'download',
                };
                Drupal.atsmarttag._addChaptersToProperties(
                  drupalSettings.atsmarttag,
                  clickProperties,
                );
                window.ATTag.click.send(clickProperties);
              }
            } else if (
              drupalSettings.atsmarttag.trackMailto &&
              link.matches("a[href^='mailto:'],area[href^='mailto:']")
            ) {
              // Mailto link clicked.
              const clickProperties = {
                elem: link,
                name: link.href,
                type: 'action',
              };
              self._addChaptersToProperties(
                drupalSettings.atsmarttag,
                clickProperties,
              );
              window.ATTag.click.send(clickProperties);
            } else if (
              drupalSettings.atsmarttag.trackOutbound &&
              link.href.match(/^\w+:\/\//i)
            ) {
              // External link clicked / No top-level cross-domain clicked.
              const clickProperties = {
                elem: link,
                name: link.href,
                type: 'navigation',
              };
              self._addChaptersToProperties(
                drupalSettings.atsmarttag,
                clickProperties,
              );
              window.ATTag.click.send(clickProperties);
            }
          });
      });
    },

    _triggerInitializedEvent() {
      $(document).trigger('atsmarttag:tag_initialized');
    },

    createTagAndDispatch(settings = drupalSettings.atsmarttag) {
      this._instantiateTag(settings);
      this._configurePrivacy(settings);
      this._processPage(settings);
      this._registerActionListeners();
      this._triggerInitializedEvent();
    },

    /**
     * Check whether this is a download URL or not.
     *
     * @param {string} url
     *   The web url to check.
     *
     * @return {boolean} isDownload
     */
    isDownload(url) {
      const isDownload = new RegExp(
        `\\.(${drupalSettings.atsmarttag.trackDownloadExtensions})([?#].*)?$`,
        'i',
      );
      return isDownload.test(url);
    },

    /**
     * Check whether this is an absolute internal URL or not.
     *
     * @param {string} url
     *   The web url to check.
     *
     * @return {boolean} isInternal
     */
    isInternal(url) {
      const isInternal = new RegExp(`^(https?)://${window.location.host}`, 'i');
      return isInternal.test(url);
    },
  };

  Drupal.behaviors.atsmarttag = {
    attach(context, settings) {
      once('atsmarttag', 'html', context).forEach(() => {
        Drupal.atsmarttag.createTagAndDispatch(settings.atsmarttag);
      });
    },
  };
})(jQuery, Drupal, once, drupalSettings);
