<?php

namespace Drupal\atsmarttag\Form;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\atsmarttag\ATSmartTagUtils;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the ATSmartTagSettingsForm form.
 *
 * @package Drupal\atsmarttag\Form
 */
class ATSmartTagSettingsForm extends ConfigFormBase {

  const UPLOAD_LOCATION = 'public://atsmarttag/';

  const ATSMARTTAG_SETTINGS = 'atsmarttag.settings';

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryCollector
   */
  protected $libraryDiscovery;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructs an ATSmartTagSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Asset\LibraryDiscoveryCollector $library_discovery
   *   The library discovery service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    LibraryDiscoveryCollector $library_discovery,
    EntityTypeManagerInterface $entity_type_manager,
    FileRepositoryInterface $file_repository,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->libraryDiscovery = $library_discovery;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $library_discovery_name = DeprecationHelper::backwardsCompatibleCall(
      \Drupal::VERSION, '11.1', fn() => 'library.discovery', fn() => 'library.discovery.collector'
    );
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get($library_discovery_name),
      $container->get('entity_type.manager'),
      $container->get('file.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'atsmarttag_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::ATSMARTTAG_SETTINGS];
  }

  /**
   * Get the configuration object.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The configuration object.
   */
  protected function getConfig() {
    return $this->config(self::ATSMARTTAG_SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['smarttag'] = [
      '#type' => 'details',
      '#title' => $this->t('SmartTag settings'),
      '#open' => TRUE,
      '#description' => $this->t('You can choose to use the internal generic SmartTag file provided by the module, or provide an external URL, or upload it locally.'),
    ];

    $form['smarttag']['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('SmartTag file location'),
      '#description' => $this->t("Choose the way you want to handle the SmartTag script file."),
      '#options' => [
        'generic' => $this->t("Generic SmartTag file provided by the module"),
        'url' => $this->t("Remote SmartTag URL"),
        'file' => $this->t("Uploaded SmartTag file"),
        'manual' => $this->t("I want to manage my smarttag.js by myself"),
      ],
      '#default_value' => $config->get('mode') ?? 'generic',
      '#required' => TRUE,
    ];

    $form['smarttag']['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site'),
      '#description' => $this->t("AT-Internet : Numéro de site"),
      '#default_value' => $config->get('site'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
      ],
    ];
    $form['smarttag']['collect_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte'),
      '#description' => $this->t("AT-Internet : Domaine de collecte des données"),
      '#default_value' => $config->get('collect_domain'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
      ],
    ];
    $form['smarttag']['collect_domain_ssl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte (SSL)'),
      '#description' => $this->t("AT-Internet : Domaine de collecte sécurisé des données"),
      '#default_value' => $config->get('collect_domain_ssl'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
      ],
    ];

    $form['smarttag']['smarttag_file_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
      ],
    ];
    $smarttag_file = $config->get('smarttag_file');
    $form['smarttag']['smarttag_file_container']['smarttag_file'] = [
      '#title' => $this->t('SmartTag file (with a .txt extension)'),
      '#type' => 'managed_file',
      '#description' => $this->t('Upload your AT Internet SmartTag file here. A .txt extension is required as Drupal will not allow .js files to be uploaded.'),
      '#default_value' => $smarttag_file ? [$smarttag_file] : NULL,
      '#upload_location' => self::UPLOAD_LOCATION,
      '#upload_validators' => [
        'file_validate_extensions' => ['txt'],
      ],
      // Sadly code below doesn't work, it's a well-known issue.
      // That's the reason we need to use a container to hide it.
      /*'#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
      ],*/
    ];

    $form['smarttag']['smarttag_url'] = [
      '#title' => $this->t('SmartTag URL'),
      '#type' => 'url',
      '#description' => $this->t('Paste the URL of your SmartTag file here.'),
      '#default_value' => $config->get('smarttag_url'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'url'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'url'],
        ],
      ],
    ];

    $form['label_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Libellé de la page'),
      '#description' => $this->t("Ce paramètre détermine comment seront intitulées les pages dans les tableaux de bord d'AT-Internet."),
      '#options' => [
        'page_title' => $this->t('Titre de la page (unicité non garantie)'),
        'path_alias' => $this->t('Chemin de la page (path alias unique)'),
      ],
      '#default_value' => $config->get('label_type') ?? 'path_alias',
    ];
    $form['chapters_from_breadcrumb'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initialize page chapters from Drupal breadcrumb'),
      '#description' => $this->t("Ce paramètre permet de renseigner les pages de niveau 1, 2 et 3 à partir des éléments du fil d'ariane Drupal (breadcrumb)."),
      '#default_value' => $config->get('chapters_from_breadcrumb') ?? 0,
    ];
    $form['secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Secure cookie (https)'),
      '#description' => $this->t("AT-Internet : ce paramètre permet d'envoyer les informations de manière sécurisée ou non. Cocher dans le cas d'un protocole HTTPS et décocher pour un protocole HTTP."),
      '#default_value' => $config->get('secure') ?? 1,
    ];
    $form['disable_cookie'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable tracker cookie'),
      '#description' => $this->t("AT-Internet : ce paramètre, si il est coché, permet d'empêcher l'écriture de cookies du tracker (first et third)."),
      '#default_value' => $config->get('disable_cookie') ?? 1,
    ];
    $form['cookie_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domaine des cookies'),
      '#description' => $this->t("AT-Internet : Domaine pour l'écriture des cookies. Si ce paramètre est vide (cookieDomain: ''), le domaine de la page courante sera utilisé."),
      '#default_value' => $config->get('cookie_domain'),
    ];
    $form['cnil_exempt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Exemption CNIL (requiert l'activation du plugin Privacy)"),
      '#description' => $this->t("AT-Internet : L'activation du mode hybride exempt de l'autorité CNIL permet une mesure restreinte par le Tracker. Seuls les paramètres jugés « strictement nécessaires » sont collectés."),
      '#default_value' => $config->get('cnil_exempt') ?? 1,
    ];

    // Link specific configurations.
    $form['linktracking'] = [
      '#type' => 'details',
      '#title' => $this->t('Links and downloads'),
      '#group' => 'tracking_scope',
    ];
    $form['linktracking']['trackoutbound'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on outbound links'),
      '#default_value' => $config->get('track.outbound') ?? 0,
    ];
    $form['linktracking']['trackmailto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on mailto links'),
      '#default_value' => $config->get('track.mailto') ?? 0,
    ];
    $form['linktracking']['trackfiles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track downloads (clicks on file links) for the following extensions'),
      '#default_value' => $config->get('track.files') ?? 0,
    ];
    $form['linktracking']['trackfiles_extensions'] = [
      '#title' => $this->t('List of download file extensions'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $config->get('track.files_extensions') ?? '',
      '#description' => $this->t('A file extension list separated by the | character that will be tracked as download when clicked. Regular expressions are supported. For example: @extensions', ['@extensions' => ATSmartTagUtils::TRACKFILES_EXTENSIONS]),
      '#maxlength' => 500,
      '#states' => [
        'enabled' => [
          ':input[name="trackfiles"]' => ['checked' => TRUE],
        ],
        // Note: Form required marker is not visible as title is invisible.
        'required' => [
          ':input[name="trackfiles"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $mode = $form_state->getValue('mode');
    $library = $this->getTagLibrary($mode);

    $file = $this->processTagFile($form_state);

    $config
      ->set('mode', $mode)
      ->set('smarttag_library', $library)
      ->set('smarttag_url', $form_state->getValue('smarttag_url'))
      ->set('smarttag_file', isset($file) ? $file->id() : NULL)
      ->set('site', $form_state->getValue('site'))
      ->set('collect_domain', $form_state->getValue('collect_domain'))
      ->set('collect_domain_ssl', $form_state->getValue('collect_domain_ssl'))
      ->set('label_type', $form_state->getValue('label_type'))
      ->set('chapters_from_breadcrumb', $form_state->getValue('chapters_from_breadcrumb'))
      ->set('secure', $form_state->getValue('secure'))
      ->set('disable_cookie', $form_state->getValue('disable_cookie'))
      ->set('cookie_domain', $form_state->getValue('cookie_domain'))
      ->set('cnil_exempt', $form_state->getValue('cnil_exempt'))
      ->set('track.outbound', $form_state->getValue('trackoutbound'))
      ->set('track.mailto', $form_state->getValue('trackmailto'))
      ->set('track.files', $form_state->getValue('trackfiles'))
      ->set('track.files_extensions', $form_state->getValue('trackfiles_extensions'))
      ->save();

    $this->libraryDiscovery->clear();

    parent::submitForm($form, $form_state);
  }

  /**
   * Processes the uploaded tag file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\file\FileInterface|null
   *   The processed file object, or NULL if no file was uploaded.
   */
  protected function processTagFile(FormStateInterface $form_state) {
    $file_value = $form_state->getValue('smarttag_file');
    if (empty($file_value) || !is_array($file_value)) {
      return NULL;
    }
    $fid = $file_value[0];
    if (empty($fid)) {
      return NULL;
    }
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return NULL;
    }
    // Rename newly uploaded file by moving or replacing it.
    $tagTargetUri = self::UPLOAD_LOCATION . $this->getTagFilename();
    if ($file->getFileUri() === $tagTargetUri) {
      return $file;
    }
    else {
      $moved_file = $this->fileRepository->move(
        $file, $tagTargetUri, FileExists::Replace);
      $moved_file->setFilename($this->getTagFilename());
      // We don't want the SmartTag file to be garbage collected.
      $moved_file->setPermanent();
      $moved_file->save();
      return $moved_file;
    }
  }

  /**
   * Generates the filename for the tag file.
   *
   * @return string
   *   The tag file filename.
   */
  protected function getTagFilename() {
    return 'smarttag.js';
  }

  /**
   * Gets the tag library based on the mode.
   *
   * @param string $mode
   *   The mode value.
   *
   * @return string
   *   The tag library string.
   */
  protected function getTagLibrary($mode) {
    switch ($mode) {
      case 'generic':
        $library = 'atsmarttag/smarttag.js';
        break;

      case 'url':
        $library = 'atsmarttag/smarttag_url';
        break;

      case 'file':
        $library = 'atsmarttag/smarttag_file';
        break;

      default:
        $library = '';
    }
    return $library;
  }

}
