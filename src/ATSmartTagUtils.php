<?php

namespace Drupal\atsmarttag;

use Drupal\file\FileInterface;

/**
 * Provides utility functions for ATSmartTag module.
 */
class ATSmartTagUtils {

  /**
   * Define the default file extension list that should be tracked as download.
   */
  const TRACKFILES_EXTENSIONS = '7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip';

  /**
   * Retrieves the library information from a URL.
   *
   * @param string $url
   *   The URL of the library.
   *
   * @return array
   *   The library information array.
   */
  public static function getLibraryInfoFromUrl(string $url): array {
    return [
      // Load the script in header.
      'header' => TRUE,
      'js' => [
        $url => [
          // Is an external URL.
          'external' => TRUE,
        ],
      ],
    ];
  }

  /**
   * Retrieves the library information from a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file object.
   *
   * @return array
   *   The library information array.
   */
  public static function getLibraryInfoFromFile(FileInterface $file): array {
    $uri = $file->getFileUri();
    $path = self::getFilePathRelativeToDrupalRoot($uri);
    return [
      // Load the script in header.
      'header' => TRUE,
      'js' => [
        $path => [
          // Preprocess allows aggregation for better performance.
          'preprocess' => TRUE,
        ],
      ],
    ];
  }

  /**
   * Retrieves the file path relative to the Drupal root.
   *
   * @param string $uri
   *   The file URI.
   *
   * @return string
   *   The file path relative to the Drupal root.
   */
  private static function getFilePathRelativeToDrupalRoot(string $uri): string {
    $path = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    return str_replace(base_path(), '/', parse_url($path, PHP_URL_PATH));
  }

}
